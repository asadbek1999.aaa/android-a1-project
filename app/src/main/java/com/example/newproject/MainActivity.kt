package com.example.newproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.newproject.databinding.ActivityMainBinding
import com.example.newproject.ui.FirstFragment
import com.example.newproject.ui.SecondFragment
import com.example.newproject.ui.ThirdFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btn1.setOnClickListener {
            supportFragmentManager.beginTransaction().replace(R.id.container, FirstFragment()).commit()
        }

        binding.btn2.setOnClickListener {
            supportFragmentManager.beginTransaction().replace(R.id.container, SecondFragment()).commit()

        }

        binding.btn3.setOnClickListener {
            supportFragmentManager.beginTransaction().replace(R.id.container, ThirdFragment()).commit()
        }
    }
}